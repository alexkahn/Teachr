var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var bcrypt = require('bcrypt');

const saltRounds = 10;

function deleteAll() {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        db.dropDatabase();
    });
}

function addUser(usrnamestr, namestr, frstnamestr, emailstr, passwordstr, addressstr) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        bcrypt.genSalt(saltRounds,
            function (err, salt) {
                bcrypt.hash(passwordstr, salt, function (err, hash) {
                    var myobj = {username: usrnamestr, name: namestr, firstname: frstnamestr, email: emailstr, password: hash, address: addressstr};
                    db.collection("users").insertOne(myobj, function (err,res) {
                        if (err) throw err;
                        console.log("Added a new user");
                        db.close();
                    });
                });
            });
    });
};

function findUser(namestr) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var query = {name: namestr};
        db.collection("users").find(query).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
        });
    });
}

function login(usrnamestr, passwdstring) {
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var query = {username: usrnamestr}
        db.collection("users").find(query).toArray(function (err, result) {
            if (err) throw err;
            var hash = result;
            console.log(hash);
            var bool = bcrypt.compare(passwdstring, hash, function (err, res) {
            });
            console.log(bool);
            if (bool) {
              return res.redirect('../views/teacher');
            }
        })
        db.close();
    })

}

//addUser('alexkahn','kahn', 'alexandre', 'a@a.be', 'password', 'asdfasdf')


//login('alexkahn','a')

