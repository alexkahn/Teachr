// Based on: https://scotch.io/tutorials/easy-node-authentication-setup-and-local#toc-application-setup-serverjs

var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var configDB = require('./config/database.js');
var path = require('path');


mongoose.connect(configDB.url);

require('./config/passport')(passport);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());

app.set('view engine', 'ejs');

app.use(session({ secret: 'ineedsomecoffeerightnow' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routes.js')(app, passport);

app.listen(port);
app.use('/css', express.static(path.join(__dirname, 'views/css')));
app.use('/img', express.static(path.join(__dirname, 'views/img')));
app.use('/js', express.static(path.join(__dirname, 'views/js')));
app.use('/fonts', express.static(path.join(__dirname, 'views/fonts')));
app.use('/data', express.static(path.join(__dirname, 'views/data')));
console.log('Server running on ' + port);
