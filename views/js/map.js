mapboxgl.accessToken = 'pk.eyJ1Ijoic2RlcHJvb3MiLCJhIjoiY2o4bGdjc2VwMGtlZTJwdWEzbGZ1dmtyYyJ9.Qldx_kCBPgGyLe0SThiZUg';
var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
    center: [-74.50, 40], // starting position [lng, lat]
    zoom: 9 // starting zoom
});
