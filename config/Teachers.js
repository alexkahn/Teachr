var MongoClient = require('mongodb').MongoClient;
var MapboxClient = require('mapbox');
var client = new MapboxClient('pk.eyJ1IjoiYWxleGthaG4iLCJhIjoiY2o4bHFzZnMzMHF1NTMycWhnNnBwb2FwNyJ9.oa8bI4FKTEwWNgJSWMGvDA'); // Mapbox token of Alexandre Kahn
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = require('./database.js').url;

function addressToCoordinates(street, number, zipcode, city, fnToDB) {
    var coordinates;
    var address = street + " " + number + " " + zipcode + " " + city;
    client.geocodeForward(address, function (err, data, res) {
        // data is the geocoding result as parsed JSON
        // res is the http response, including: status, headers and entity properties
        coordinates = data.features[0].geometry.coordinates;
        fnToDB(coordinates);
    });
};

module.exports = {
    insertToDB: function insertToDB(username, firstName, secondName, street, number, zipcode, city, topic, target, experience) {
        function insertData(coordinates) {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var userdata = {
                    "username": username,
                    "firstname": firstName,
                    "secondname": secondName,
                    "address": {
                        "street": street,
                        "number": number,
                        "zipcode": zipcode,
                        "city": city
                    },
                    "location": {"type": "Point", "coordinates": coordinates},
                    "topic": topic,
                    "target": target,
                    "experience": experience
                };
                db.collection("teachers").insert(userdata, function (err, res) {
                    if (err) throw err;
                    console.log("1 document inserted");
                    db.close();
                });
            });
        };
        addressToCoordinates(street, number, zipcode, city, insertData);
    },
    findUsername: function findUsername(username, callbackFn) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var query = {"username": username};
            db.collection("teachers").find(query).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                callbackFn(result[0]);
                db.close();
            });
        });
    },
    findTeacher: function findTeacher(username, callbackFn) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var query = {"username": username};
            db.collection("teachers").find(query).toArray(function (err, result) {
                var boolean = true;
                if (err) boolean = false;
                console.log(result);
                callbackFn(boolean);
                db.close();
            });
        });
    },
    teacherLookup: function teacherLookup(myLatitude, myLongitude, topic, callbackFn) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            db.collection("teachers").find({
                $and:
                    [
                        {
                            location: {
                                $nearSphere: {
                                    $geometry: {
                                        type: "Point",
                                        coordinates: [Number(myLatitude), Number(myLongitude)]
                                    },
                                    $maxDistance: 200000000
                                }
                            }
                        },
                        {topic: {$eq: topic}}
                    ]
            }).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                callbackFn(result);
                db.close();
            });
        });
    },
    updateTeacher: function updateTeacher(username, firstName, secondName, street, number, zipcode, city, topic, target, experience) {
        function updateData(coordinates) {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var query = {"username": username};
                var newvalues = {
                    "username": username,
                    "firstname": firstName,
                    "secondname": secondName,
                    "address": {
                        "street": street,
                        "number": number,
                        "zipcode": zipcode,
                        "city": city
                    },
                    "location": {"type": "Point", "coordinates": coordinates},
                    "topic": topic,
                    "target": target,
                    "experience": experience
                };
                db.collection("teachers").update(query, newvalues, function (err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    db.close();
                });
            });
        }
        addressToCoordinates(street, number, zipcode, city, updateData);
    }
};

// Code needed to index the database correctly

// MongoClient.connect(url, function (err, db) {
//     if (err) throw err;
//     db.collection('teachers').createIndex({location: "2dsphere"});
//     db.close();
// });

