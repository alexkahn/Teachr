// app/routes.js

var teachers  = require('../config/Teachers.js');
var courses = require('../config/courses.js');
var reviews = require('../config/Reviews');
var XMLHttpRequest = require("../node_modules/xmlhttprequest").XMLHttpRequest;



module.exports = function(app, passport) {


    // INDEX //
    app.get('/',  function(req, res) {
        var loggedIn = 0;
        var user = null;
        var isTeacher = null;
        if (req.isAuthenticated()) {
            loggedIn = 1;
            user = req.user.local.email
            if (req.user.local.isTeacher){
                isTeacher = 1
            }
        }
        res.render('index',  {
            title: "Teachr home - Vind bijles in jouw buurt",
            loggedIn: loggedIn,
            user: user,
            isTeacher: isTeacher
        });
    });


    // STUDENT //
    app.get('/student', function(req,res){

        res.render('studentProfile.ejs', {
            title: "Teacher student portal"
        });
    });

    app.get('/myprofile',isLoggedIn, function(req, res){
        var teacher = req.user.local.email;
        var mail;
        var firstname;
        var secondname;
        var topic;
        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }

        teachers.findUsername(teacher,getUserReviews);
        function getUserReviews(result) {
            title: "My profile"
            mail = teacher;
            firstname = result.firstname;
            secondname = result.secondname;
            topic = result.topic;
            reviews.getReviews(teacher,renderProfile)
        }
        function renderProfile(result) {
            var reviews = [];
            function collectReviews(item) {
                reviews.push({rev: item.review});
            }
            if (result.length > 0) {
                //reviews = [{rev: result[0].review}, {rev: result[1].review}];
                result.forEach(collectReviews);
            }
            else {
                reviews = [{rev: "No reviews yet"}];
            }
            res.render('selfProfile.ejs', {
                    //title: teacher,
                    user: teacher,
                    mail: mail,
                    firstname: firstname,
                    secondname: secondname,
                    topic: topic,
                    reviews: reviews,
                    isTeacher: isTeacher
                }
            );
        }
    });


    // TEACHER //
    app.get('/teacher', isLoggedIn, function(req, res){
        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }
        var user = req.user.local.email;
        res.render('teacher.ejs', {
            title: "Search a teacher",
            course: null,
            user: user,
            isTeacher: isTeacher
        });
    });

    app.get('/teacher/course/:course',isLoggedIn, function(req, res){
        var course = req.params.course;
        var user = req.user.local.email;
        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }
        res.render('teacher.ejs', {
            title: "Teacher student portal",
            user: user,
            course: course,
            isTeacher:1
        });
    });


    // SIGNUP //
    app.get('/signup/signup/:signup', function(req, res){
        var signup = req.params.signup;
        if (signup == "true" ) {
            signup = 1;
        }
        res.render('signup.ejs', {
            title: "Sign up to Teachr",
            message: req.flash('loginMessage'),
            signup: signup
        });
    });


    // TEACHER PROFILE //
    app.get('/teacherProfile/username/:username', isLoggedIn, function(req, res){
        var teacher = req.params.username;
        var mail;
        var firstname;
        var score;
        var secondname;
        var topic;
        var user = req.user.local.email;
        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }
        teachers.findUsername(teacher,getUserReviews);
        function getUserReviews(result) {
            mail = teacher;
            firstname = result.firstname;
            secondname = result.secondname;
            topic = result.topic;
            reviews.getReviews(teacher,renderProfile)
        }
        function renderProfile(result) {
            var reviews = [];
            function collectReviews(item) {
                reviews.push({rev: item.review});
            }
            if (result.review.length > 0) {
                result.review.forEach(collectReviews);
                score = result.score / 10;
                console.log(score);
            }
            else {
                score = 3;
                reviews = [{rev: "No reviews yet"}];
            }
            res.render('selfprofile.ejs', {
                    user: teacher,
                    title: teacher,
                    mail: mail,
                    firstname: firstname,
                    secondname: secondname,
                    topic: topic,
                    reviews: reviews,
                    score: score,
                    isTeacher: isTeacher,
                    user: user
                }

            );
        }
    });


    // SETTINGS //
    app.get('/settings', isLoggedIn, function(req, res){
        var user = req.user.local.email;
        var update = 1;
        teachers.findUsername(user, toJSON);
        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }
        function toJSON(result) {
            //console.log(result.username);



            if (result) {
                var first = result.firstname;
                var second = result.secondname;
                var street = result.address.street;
                var number = result.address.number;
                var zip = result.address.zipcode;
                var city = result.address.city;
                var topic = result.topic;
                var target = result.target;

                console.log("Update user");
                res.render('update.ejs', {
                    title: "Settings",
                    firstname: first,
                    secondname: second,
                    street: street,
                    number: number,
                    zip: zip,
                    city: city,
                    topic: topic,
                    target: target,
                    user: user,
                    isTeacher: isTeacher
                });
            }

            else {
                res.render('settings.ejs', {
                    title: "Settings",
                    user: user,
                    isTeacher: isTeacher
                });
            }
        }

    });

    // REVIEWS //
    app.get('/reviews/teacher/:teacher', isLoggedIn, function(req, res) {
        var teacher = req.params.teacher;
        var user = req.user.local.email;

        var isTeacher = 0;
        if (req.user.local.isTeacher){
            isTeacher = 1;
        }

        var name;
        teachers.findUsername(teacher, getInfo);

        function getInfo(item){
            name = item.firstname;
            console.log(item.firstname);

            res.render('review.ejs',{
                title: "Reviews " + name,
                teacher: name,
                username: teacher,
                user: user,
                isTeacher: isTeacher
            });
        }


    });

    // IMAGES //
    app.get('/img', function (req, res) {
        res.json(courses.courses);
    });


    // API //
    app.get('/api', function (req, res) {
        res.json({message: 'Hooray! welcome to our api!'})
    });


    // TEACHER LOOK UP //
    app.get('/api/long/:longitude/lat/:latitude/topic/:topic', function (req, res) {
        function toJSON(result) {
            res.json(result);
        }
        teachers.teacherLookup(req.params.latitude, req.params.longitude, req.params.topic, toJSON);
    });


    // COURSES //
    app.get('/api/courses', function (req, res) {
        res.json(courses.courses);
    });

    // INSERT //
    app.post('/api/updateToDB', isLoggedIn, function (req, res) {
        var username = req.user.local.email;
        var first = req.body.firstname;
        var second = req.body.secondname;
        var street = req.body.street;
        var number = req.body.number;
        var zip = req.body.zipcode;
        var city = req.body.city;
        var topic = req.body.topic;
        var target = req.body.target;
        var exp = req.body.exp;
        console.log(topic);
        teachers.updateTeacher(username, first,second,street,number,zip,city,topic,target,exp);
        res.redirect('/settings');
        res.end('It worked!');;
    });

    app.post('/api/insertToDB', isLoggedIn, function (req, res) {
        var username = req.user.local.email;
        var first = req.body.firstname;
        var second = req.body.secondname;
        var street = req.body.street;
        var number = req.body.number;
        var zip = req.body.zipcode;
        var city = req.body.city;
        var topic = req.body.topic;
        var target = req.body.target;
        var exp = req.body.exp;
        teachers.insertToDB(username, first,second,street,number,zip,city,topic,target,exp);
        res.redirect('/settings');
        res.end('It worked!');
    });


    app.get('/registercheck', isLoggedIn, function (req, res) {
        console.log("register")
        console.log(req.user.local.isTeacher);
        if (req.user.local.isTeacher){
            res.redirect('/settings');
        }
        else{
            res.redirect('/teacher')
        }

    });


    app.post('/register',  passport.authenticate('local-signup', {
        successRedirect : '/registercheck', // redirect to the secure profile section
        failureRedirect : '/signup/signup/true', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/teacher', // redirect to the secure profile section
        failureRedirect : '/signup/signup/false', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));



    app.post('/reviews/teacher/:teacher', function (req, res) {
        var teacher = req.params.teacher;
        var score = req.body.score;
        var review = req.body.review;
        reviews.addReview(teacher, score, review);
        console.log(req.body.review);
        res.redirect('/teacher');
        res.end('It worked!');;
    });



    // REVIEWS //
    app.get('/reviews/teacher/:teacher', function(req, res) {
        var teacher = req.params.teacher;
        var user = req.user.local.email
        var name;
        teachers.findUsername(teacher, getInfo);

        function getInfo(item){
            name = item.firstname;
            console.log(item.firstname)

            res.render('review.ejs',{
                title: "Reviews " + name,
                teacher: name,
                username: teacher,
                user: user
            });
        }


    });


    app.get('/api/reviews/teacher/:teacher', function (req, res) {

        function toJSON(result) {
            res.json(result);
        }
        var teacher = req.params.teacher;
        reviews.getReviews(teacher, toJSON);
    });

    app.get('/api/score/teacher/:teacher', function (req, res) {
        function toJSON(result) {
            res.json([result]);
        }
        var teacher = req.params.teacher;
        reviews.getScore(teacher, toJSON);
    });

    app.get('/logout', function (req, res) {
            req.session.destroy(function (){
                res.redirect('/')
            })
        }
    )
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/signup/signup/false');
}
