var courses = [
    {
        "title":"Wiskunde",
        "description": "Lager onderwijs",
        "en": "math"
    },
    {
        "title":"Wiskunde",
        "description": "1ste graad",
        "en": "math"
    },

    {
        "title":"Wiskunde",
        "description": "2de graad",
        "en": "math"
    },

    {
        "title":"Wiskunde",
        "description": "3de graad",
        "en": "math"
    },
    {
        "title":"Wiskunde",
        "description": "Hoger onderwijs",
        "en": "math"
    },
    {
        "title":"Wiskunde",
        "description": "Universitair",
        "en": "math"
    },
    {
        "title":"Fysica",
        "description": "Lager onderwijs",
        "en": "physics"
    },
    {
        "title":"Fysica",
        "description": "1ste graad",
        "en": "physics"
    },
    {
        "title":"Fysica",
        "description": "2de graad",
        "en": "physics"
    },
    {
        "title":"Fysica",
        "description": "3de graad",
        "en": "physics"
    },

    {
        "title":"Fysica",
        "description": "Hoger onderwijs",
        "en": "physics"
    },
    {
        "title":"Fysica",
        "description": "Universitair",
        "en": "physics"
    },
    {
        "title":"Chemie",
        "description": "Lager onderwijs",
        "en": "chemistry"
    },
    {
        "title":"Chemie",
        "description": "1ste graad",
        "en": "chemistry"
    },

    {
        "title":"Chemie",
        "description": "2de graad",
        "en": "chemistry"
    },

    {
        "title":"Chemie",
        "description": "3de graad",
        "en": "chemistry"
    },
    {
        "title":"Chemie",
        "description": "Hoger onderwijs",
        "en": "chemistry"
    },
    {
        "title":"Aardrijkskunde",
        "description": "1ste graad",
        "en": "geography"
    }
];
exports.courses = courses;
