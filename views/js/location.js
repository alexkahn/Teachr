var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

getGeoLocation();
var current_position = null;


function success(pos) {
    current_position = pos.coords;
    // Debug
   /* console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);
    return(crd);*/
};

function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
};

function getGeoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, options);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}