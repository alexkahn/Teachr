var MongoClient = require('mongodb').MongoClient;

var url = require('./database.js').url;

var score = 0;

function average(item) {
    score = item.score + score;
}

module.exports = {
    addReview: function addReview(teacher, score, reviewtext) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            if (score > 5) throw error("Score to big, should be less than 5");
            if (score < 0) throw error("Score to small, should be more than 0");
            console.log(reviewtext);
            var review = {
                "score": score,
                "review": reviewtext,
                "teacher": teacher
            };
            db.collection("reviews").insertOne(review, function (err, res) {
                if (err) throw err;
                console.log("Added a review");
                db.close();
            });
        });
    },
    getReviews: function getReviews(teacher, callbackFn) {
        score = 0;
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            db.collection("reviews").find({"teacher": {$eq: teacher}}).toArray(function (err, result) {
                if (err) throw err;
                result.forEach(average);
                score = score / result.length;
                result = {
                    "score": score,
                    "review":result
                };
                callbackFn(result);
                db.close();
            });

        });
    },
    getScore: function getScore(teacher, callbackFn) {
        score = 0;
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            db.collection("reviews").find({"teacher": {$eq: teacher}}).toArray(function (err, result) {
                if (err) throw err;
                result.forEach(average);
                score = score / result.length;
                callbackFn(score);
                db.close();
            });

        });
    }
}
